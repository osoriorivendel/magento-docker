FROM osoriorivendel/magento
ADD install-magento /usr/local/bin/
RUN chmod +x /usr/local/bin/install-magento
#RUN /etc/init.d/nginx stop
ENTRYPOINT service nginx start && sleep 30 && sh /usr/local/bin/install-magento && service nginx stop && service php7.0-fpm start && nginx -g 'daemon off;'
